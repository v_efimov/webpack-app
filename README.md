# webpack-app

1. Создать отдельный репозиторий webpack-app
2. Создать ветку feature/hw-01
3. В папке src создать произвольную простую веб-страницу (index.html со стилем и скриптом)
4. Подключить webpack, создать конфиг для webpack.
5. Настроить сборку проекта через webpack-dev-server.
6. Добавить скрипт start, запускающий dev-сервер в package.json
7. Подключить и настроить плагины: HtmlWebpackPlugin, MiniCssExtractPlugin, ESLintPlugin, StylelintPlugin
8. Продемонстрировать что в js и css коде есть ошибки
9. Для обработки css использовать лоадеры MiniCssExtractPlugin.loader, css-loader
10. Для обработки js использовать babel-loader
11. Открыть Pull Request feature/hw-01 -> master
12. Отдать Pull Request менторам на проверку
